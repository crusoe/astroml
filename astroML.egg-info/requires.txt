scikit-learn>=0.18
numpy>=1.13
scipy>=0.18
matplotlib>=3.0
astropy>=3.0

[all]
pymc3<3.11,>=3.7

[codestyle]
flake8

[docs]
sphinx

[test]
pytest-doctestplus
pytest-astropy-header
pytest-remotedata
pytest-cov
